$(document).on(
   "mobileinit",
   function()
   {
      $.mobile.defaultPageTransition = "slide";
      
      // Loader
      $.mobile.loader.prototype.options.text = "Caricamento...";
      $.mobile.loader.prototype.options.textVisible = true;
      $.mobile.loader.prototype.options.theme = "a";

      // Theme
      $.mobile.page.prototype.options.theme  = "e";
      $.mobile.page.prototype.options.headerTheme = "a";
      $.mobile.page.prototype.options.contentTheme = "e";
      $.mobile.page.prototype.options.footerTheme = "a";
      $.mobile.page.prototype.options.backBtnTheme = "e";
   }
);