var Utility = {
   onDeviceReady: function()
   {
      Utility.initIndex();
      $.mobile.loading("hide");
      $("#cerca-pagina").live("pageinit", Utility.initRicerca);
      $("#home-pagina").live(
         "pagebeforeshow",
         function(event)
         {
            $("#da, #a").val("");
         }
      );
      $("#home-pagina").live(
         "pageinit",
         function()
         {
            $.mobile.loading("show");
         }
      );
   },
   initIndex: function()
   {
      $("#da-link, #a-link").click(function(event) {
         $.mobile.loading("show");
         var addressId = this.id.substring(0, this.id.indexOf("-"));

         navigator.geolocation.getCurrentPosition(function(position) {
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({
               "location": new google.maps.LatLng(position.coords.latitude, position.coords.longitude)
            },
            function(results, status) {
               $.mobile.loading("hide");

               if (status == google.maps.GeocoderStatus.OK)
                  $("#" + addressId).val(results[0].formatted_address);
               else
                  navigator.notification.alert(
                     "Geolocalizzazione fallita. Impossibile recuperare la tua posizione",
                     function(){},
                     "Errore"
                  );
            });
         },
         function(error){
            $.mobile.loading("hide");
            navigator.notification.alert(
               "Geolocalizzazione fallita. Impossibile recuperare la tua posizione",
               function(){},
               "Errore"
            );
         });
      });
   },
   initRicerca: function()
   {
      $.getJSON(
         "database/database.json",
         function(data) {
            map = new AuderoMap(data, "map");
            map.calculateRoute(0);
            map.displayMap("map");
         }
      );
      $(".percorso").on(
         "expand",
         function() {
            var routeNumber = parseInt($(this).attr("id").match(/\d/g));
            map.calculateRoute(routeNumber);
         }
      );
   }
}